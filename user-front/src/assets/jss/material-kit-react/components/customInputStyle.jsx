// ##############################
// // // CustomInput styles
// #############################

import {
  primaryColor,
  dangerColor,
  successColor,
  defaultFont
} from "assets/jss/material-kit-react.jsx";

const customInputStyle = {
  disabled: {
    "&:before": {
      backgroundColor: "transparent !important"
    }
  },
  underline: {
    "&:hover:not($disabled):before,&:before": {
      backgroundColor: "#31B404",
      height: "1px !important"
    },
    "&:after": {
      backgroundColor: primaryColor
    }
  },
  underlineError: {
    "&:after": {
      backgroundColor: dangerColor
    }
  },
  underlineSuccess: {
    "&:after": {
      backgroundColor: successColor
    }
  },
  labelRoot: {
    ...defaultFont,
    color: "#31B404 !important",
    fontWeight: "400",
    fontSize: "14px",
    lineHeight: "1.42857"
  },
  labelRootError: {
    color: dangerColor + " !important"
  },
  labelRootSuccess: {
    color: successColor + " !important"
  },
  feedback: {
    position: "absolute",
    top: "18px",
    right: "0",
    zIndex: "2",
    display: "block",
    width: "24px",
    height: "24px",
    textAlign: "center",
    pointerEvents: "none"
  },
  marginTop: {
    marginTop: "16px"
  },
  formControl: {
    paddingBottom: "7px",
    margin: "27px 0 0 0",
    position: "relative"
  },
  formControlLabel: {
    margin: "11px 0 0 0",
  },
  whiteUnderline: {
    "&:hover:not($disabled):before,&:before": {
      backgroundColor: "#31B404"
    },
    "&:after": {
      backgroundColor: "#31B404"
    }
  },
  input: {
    "&,&::placeholder": {
      color: "#31B404",
      fontSize: "14px",
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "400",
      lineHeight: "1.42857",
      opacity: "1"
    }
  },
  whiteInput: {
    "&,&::placeholder": {
      color: "#31B404",
      opacity: "1"
    }
  }
};

export default customInputStyle;
