import React from "react";
// material-ui components
import withStyles from "material-ui/styles/withStyles";

// @material-ui/icons
import Cloud from "@material-ui/icons/Cloud";
import Device from "@material-ui/icons/DeviceHub";
import Security from "@material-ui/icons/Security";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";

import productStyle from "assets/jss/material-kit-react/views/landingPageSections/productStyle.jsx";

class ProductSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h2 className={classes.title}>Núcleo de Pesquisa e Desenvolvimento</h2>
            <h5 className={classes.description}>
              O NPD - Núcleo de Pesquisa e Desenvolvimento da Faculdade de Sinop - Fasipe,
              tem como principal objetivo, criar, divulgar, e integradar pesquisas e soluções
              que auxilam no aprendizado dos alunos e no gerenciamento do campus 
            </h5>
          </GridItem>
        </GridContainer>
        <div>
          <GridContainer>
            <GridItem xs={5} sm={12} md={4}>
              <InfoArea
                title="I.A"
                description="Projetos criados utilizando tecnologias de ponta em Inteligência Artificial e processamento em nuvem."
                icon={Cloud}
                iconColor="info"
                vertical
              />
            </GridItem>
            <GridItem xs={5} sm={12} md={4}>
            <InfoArea
              title="I.A"
              description="Projetos criados utilizando tecnologias de ponta em Inteligência Artificial e processamento em nuvem."
              icon={Cloud}
              iconColor="info"
              vertical
            />
          </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <InfoArea
                title="Integrações"
                description="Todos os projetos são pensados e desenvolvidos para obter uma integração máxima entre aplicações, máximizando o desempenho e a reutilização"
                icon={Device}
                iconColor="success"
                vertical
              />
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <InfoArea
                title="Segurança"
                description="Utilizamos as mais novas tecnologias e padrões de desenvolvimento, tudo para fornecer um alto grau de segurança e confiabilidade das aplicações"
                icon={Security}
                iconColor="danger"
                vertical
              />
            </GridItem>
          </GridContainer>
        </div>
      </div>
    );
  }
}

export default withStyles(productStyle)(ProductSection);
