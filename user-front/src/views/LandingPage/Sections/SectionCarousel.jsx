import React from "react";
import { Link } from "react-router-dom";

import Carousel from "react-slick";
import withStyles from "material-ui/styles/withStyles";
import LocationOn from "@material-ui/icons/LocationOn";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import carouselStyle from "assets/jss/material-kit-react/views/componentsSections/carouselStyle.jsx";


class SectionCarousel extends React.Component {
  render() {
    const { classes } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true
    };
    return (
      <div className={classes.section}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={20} className={classes.marginAuto}>
              <Card carousel>
                <Carousel {...settings}>
                  <div>
                    <img
                      src="http://s3.amazonaws.com/porvir/wp-content/uploads/2017/09/04125935/inteligenciaartificialnaeducacao.jpg"
                      alt="First slide"
                      className="slick-image"
                    />
                    <div className="slick-caption">
                    <Link to="http://educacao.estadao.com.br/blogs/ana-maria-diniz/o-futuro-da-inteligencia-artificial-na-educacao/" target="_blank"  >
                     <h2>
                       <font color="white"> O futuro da inteligência artificial na Educação </font>
                    </h2>
                    </Link>
                    </div>
                  </div>
                  <div>
                    <img
                      src="http://noticias.universia.pt/pt/images/docentes/d/di/dic/dicas-para-memorizar-mais-rapidamente-noticias.png"
                      alt="Second slide"
                      className="slick-image"
                    />
                    <div className="slick-caption">
                    <Link to="http://noticias.universia.pt/destaque/noticia/2013/12/10/1068832/9-dicas-memorizar-mais-rapidamente.html" target="_blank"  >
                     <h2>
                       <font color="white"> 9 dicas para memorizar mais rapidamente </font>
                    </h2>
                    </Link>
                    </div>
                  </div>
                  <div>
                    <img
                      src="https://ichef.bbci.co.uk/news/660/cpsprodpb/AADA/production/_100983734_d_pedro_i_oficial.jpg"
                      alt="Third slide"
                      className="slick-image"
                    />
                    <div className="slick-caption">
                    <Link to="http://www.bbc.com/portuguese/geral-43857602?SThisFB" target="_blank"  >
                     <h3><font color="white">Inédita reconstituição facial mostra homem com nariz deformado 
                      em decorrência de suposta fratura não tratada e nunca antes descrita na literatura especializada | Crédito: Cícero Moraes</font>
                    </h3>
                    </Link>
                    </div>
                  </div>
                </Carousel>
              </Card>
            </GridItem>
          </GridContainer>
        </div>
      </div>
    );
  }
}

export default withStyles(carouselStyle)(SectionCarousel);
