import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui components
import withStyles from "material-ui/styles/withStyles";

// @material-ui/icons
import Search from "@material-ui/icons/Search";
// core components
import InputAdornment from "material-ui/Input/InputAdornment";
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import LoginPage from "../LoginPage/LoginPage"
import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";

// Sections for this page
import SectionCarousel from "./Sections/SectionCarousel.jsx"
import ProductSection from "./Sections/ProductSection.jsx";
import WorkSection from "./Sections/WorkSection.jsx";

const inicio = []

class LandingPage extends React.Component {
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          route={inicio}
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={10}>
                <h1 className={classes.title}>FASIPE Ouvidoria</h1>
                <br />
              </GridItem>
              <GridItem xs={12} sm={12} md={10} align="center">
              <CustomInput
              labelText="Pesquisar no site"
              id="material"
              formControlProps={{
                  fullWidth: true
              }}
              inputProps={{
                  endAdornment: (<InputAdornment position="end"><Search/></InputAdornment>)
              }}
               />
              </GridItem>
            </GridContainer>
          </div>

        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <ProductSection />
            <WorkSection />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(landingPageStyle)(LandingPage);
